const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const minifyjs = require("gulp-js-minify");
const uglify = require("gulp-uglify");
const pipeline = require("readable-stream").pipeline;
const imagemin = require("gulp-imagemin");
const browserSync = require("browser-sync").create();
const clean = require("gulp-clean");

function cleanDist() {
  return gulp.src("./dist/tmp", { allowEmpty: true }).pipe(clean());
}
function browserSyncFunc() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
}
function watcher() {
  gulp.watch("./src/scss/**/*.scss", gulp.parallel(buildStyles)).on("change", browserSync.reload);
  gulp.watch("./src/img/**/*", gulp.parallel(minImg)).on("change", browserSync.reload);
  gulp.watch("./index.html").on("change", browserSync.reload);
  gulp.watch("./src/script/**/*.js", gulp.parallel(buildJs)).on("change", browserSync.reload);
}


async function minImg() {
  gulp.src("./src/img/*").pipe(imagemin()).pipe(gulp.dest("./dist/img"));
}

async function buildStyles() {
  return gulp
    .src("./src/scss/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
      }),
    )
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(gulp.dest("./dist/css"));
}
async function buildJs() {
  gulp
    .src("./src/script/**/*.js")
    .pipe(minifyjs())
    .pipe(gulp.dest("./dist/js"));
}

gulp.task("build", gulp.series(cleanDist, buildStyles, buildJs, minImg));
gulp.task("dev", gulp.series(gulp.parallel(watcher, browserSyncFunc)));
